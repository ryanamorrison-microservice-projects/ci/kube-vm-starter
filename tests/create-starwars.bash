#!/bin/bash
wget https://raw.githubusercontent.com/cilium/cilium/HEAD/examples/minikube/http-sw-app.yaml
kubectl create -f http-sw-app.yaml
kubectl apply -f allow-empire-in-namespace.yaml
kubectl apply -f xwing-dns-deny-policy.yaml
sleep 15 #let the pods spinup before running the policy test script (AKA "starwars.bash")


