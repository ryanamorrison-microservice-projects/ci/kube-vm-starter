#!/bin/bash

#script defaults
VAR_GROUP="libvirt-qemu"
VAR_IMAGE_DIR="/var/lib/libvirt/images"
VAR_TEMPLATE_DIR="/var/lib/libvirt/templates"


#set user defaults
VAR_VM_NAME="vm"
VAR_USERNAME="$USER"
VAR_IMAGE_URL="https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img"
VAR_OS="ubuntu22.04"
VAR_NET_BRIDGE="br0"
VAR_MEM="2048"
VAR_CPU="2"
VAR_DNS1="1.1.1.1"
VAR_DNS2="8.8.8.8"
VAR_DISK_SIZE_IN_GB="16"
VAR_GENERATE_KEY="true"
VAR_FORCE_VM_CREATION="false"
VAR_ADMIN_GROUP="service-admins"

#help display function
usage() {
  echo -e "Usage: $0 [-h] [-b bridge] [-c cpus] [-d DNS] [-D DNS] [-f force] [-G gateway] [-i image] [-I IPV4] [-m memory] [-n name] [-o os] [-p pass] [-r disk] [-s ssh] [-u user] \n"
  echo -e "  -h: display this help message\n"
  echo -e "  -b: network bridge name (e.g., br0)"
  echo -e "      assumes a host network bridge in the host's netplan file, not a libvirt defined bridge like virbr0"
  echo -e "      defaults to example provided\n"
  echo -e "  -c: number of cpus (e.g., 2)"
  echo -e "      defaults to: 2\n"
  echo -e "  -d: first DNS server (e.g., 1.1.1.1)"
  echo -e "      defaults to example provided"
  echo -e "  -D: second DNS server (e.g., 8.8.8.8)"
  echo -e "      defaults to example provided\n"
  echo -e "  -f: force new VM creation even if it already exists\n"
  echo -e "  -G: IPv4 gateway address (e.g., 192.168.1.1)"
  echo -e "      no default\n"
  echo -e "  -i: a 'quoted' url of cloud image to be downloaded as base image"
  echo -e "      e.g., 'https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img'"
  echo -e "      defaults to example provided\n"
  echo -e "  -I: IPv4 address and CIDR subnet for default network adapter (e.g., 192.168.1.20/24)"
  echo -e "      if not specified DHCP is assumed\n"
  echo -e "  -m: memory in MB (e.g., 2048)"
  echo -e "      defaults to: 2048\n"
  echo -e "  -n: name of host, the libvirt 'domain' (e.g., myhost)"
  echo -e "      defaults to: vm\n"
  echo -e "  -o: os variant name"
  echo -e "      defaults to: ubuntu22.04\n"
  echo -e "  -p: a 'quoted' hashed password string"
  echo -e "      (e.g., '\$6\$b27iVmFzDTsiL+Cy\$ZR...')"
  echo -e "      no default\n"
  echo -e "      various utilities can generate a password such as:"
  echo -e '      `openssl passwd -6 -salt $(openssl rand -base64 16)`'
  echo -e "  -r: VM root disk size in Gigabytes (e.g. 20)"
  echo -e "      defaults to: 16\n"
  echo -e "  -s: a 'quoted' public ssh key for the default user (e.g., 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDZ...')"
  echo -e "      if not specified, password authentication will be allowed"
  echo -e "      no default\n"
  echo -e "  -u: username of the initial user on the system"
  echo -e "      defaults to user account running script\n"
}

#get args
while getopts ":b:c:d:D:G:h:i:I:m:n:o:p:r:s:u:" opt; do
  case $opt in
    b ) VAR_NET_BRIDGE="$OPTARG" 
    ;;
    c ) VAR_CPU="$OPTARG"
    ;;
    d ) VAR_DNS1="$OPTARG"
    ;;
    D ) VAR_DNS2="$OPTARG"
    ;;
    G ) VAR_GW4="$OPTARG"
    ;;
    h ) usage
        exit 0
    ;;
    i ) VAR_IMAGE_URL="$OPTARG"
    ;;
    I ) VAR_IPV4="$OPTARG"
    ;;
    m ) VAR_MEM="$OPTARG"
    ;;
    n ) VAR_VM_NAME="$OPTARG"
    ;;
    o ) VAR_OS="$OPTARG"
    ;;
    p ) VAR_PASSWORD="$OPTARG"
    ;;
    r ) VAR_DISK_SIZE_IN_GB="$OPTARG" 
    ;;
    s)  VAR_SSH_KEY="$OPTARG"
    ;;
    u ) VAR_USERNAME="$OPTARG"
    ;;
   \? ) echo "Invalid option -$OPTARG" 1>&2
        usage
        exit 1
    ;;
    #: ) echo "Invalid option: -$OPTARG requires a value" 1>&2
    #    usage
    #    exit 1
    #;;
  esac
done

#determine os family for networking configuration
if [[ $VAR_OS =~ "ubuntu" ]]; then
  VAR_OS_FAM="ubuntu"
  VAR_DEF_ADAPTER_NAME="enp1s0"
  VAR_USERDATA_FILE="$VAR_IMAGE_DIR/$VAR_VM_NAME/user-data.yaml"
  VAR_NETCONFIG_FILE="$VAR_IMAGE_DIR/$VAR_VM_NAME/network-config.yaml"
elif [[ $VAR_OS =~ "fedora" ]]; then
  VAR_OS_FAM="rhel"
  #VAR_DEF_ADAPTER_NAME="enp1s0"
  VAR_DEF_ADAPTER_NAME="eth0"
  VAR_USERDATA_FILE="$VAR_IMAGE_DIR/$VAR_VM_NAME/cloud.cfg"
  VAR_NETCONFIG_FILE=""
else
  VAR_OS_FAM="error"
fi

#check for existing VM (do not clobber)
#made to work with Gitlab's `when: manual` syntax for extended testing
VAR_VM_CHECK=$(virsh list --all | grep $VAR_VM_NAME)
if [ "$VAR_VM_CHECK" != "" ]; then
  virsh list --all | grep $VAR_VM_NAME
  echo "Found $VAR_VM_NAME, skipping creation..."
else
  echo "No existing image found, creating..."
  #set paths
  VAR_VM_DIR="$VAR_IMAGE_DIR/$VAR_VM_NAME"
  
  #get image variable from URL
  VAR_IMAGE=$(echo $VAR_IMAGE_URL | awk -F'/' '{ print $NF }')

  #check for a created ssh key, do not override one 
  #supplied by the commandline
  if [ -f $HOME/.ssh/${VAR_VM_NAME}.pub ]  && [ "$VAR_SSH_KEY" == "" ]; then
    echo "No supplied public ssh key."
    echo "Public key found at $HOME/.ssh/${VAR_VM_NAME}.pub"
    VAR_SSH_KEY=$(cat $HOME/.ssh/${VAR_VM_NAME}.pub)
  fi

  #ensure credentials so the VM's can be used
  if [ "$VAR_SSH_KEY" != "" ]; then
    echo "ssh key found, using $VAR_SSH_KEY" 
  else
    echo "no ssh key found, using passwords"
    if [ "$VAR_PASSWORD" == "" ]; then
      echo "ERROR: no password supplied"
      echo "exiting..."
      exit 1
    fi
  fi

  #download image from URL
  if [ ! -f $VAR_TEMPLATE_DIR/$VAR_IMAGE ]; then
    echo "downloading image.."
    wget  $VAR_IMAGE_URL -P $VAR_TEMPLATE_DIR
  else
    echo "image $VAR_TEMPLATE_DIR/$VAR_IMAGE found"
  fi
  chgrp $VAR_GROUP $VAR_TEMPLATE_DIR/$VAR_IMAGE
  ls -lah $VAR_TEMPLATE_DIR/$VAR_IMAGE

  #create a directory for VM files
  if [ ! -d $VAR_VM_DIR ]; then
    mkdir $VAR_VM_DIR
  fi
  chgrp  $VAR_GROUP $VAR_VM_DIR
  ls -lah $VAR_VM_DIR

  #create VM root disk from image
  if [ ! -f $VAR_VM_DIR/root-disk.qcow2 ]; then
    echo "converting image..."
    qemu-img convert -f qcow2 -O qcow2 $VAR_TEMPLATE_DIR/$VAR_IMAGE $VAR_VM_DIR/root-disk.qcow2
    echo "resizing image..."
    qemu-img resize $VAR_VM_DIR/root-disk.qcow2 ${VAR_DISK_SIZE_IN_GB}G
  fi
  chgrp $VAR_GROUP $VAR_VM_DIR/root-disk.qcow2
  ls -lah $VAR_VM_DIR/root-disk.qcow2

  if [ -f $VAR_USERDATA_FILE ]; then
    echo "found previous user data cloud init file, re-creating..."
    rm $VAR_USERDATA_FILE
  fi

  if [ -f $VAR_NETCONFIG_FILE ]; then
    echo "found previous network cloud init file, re-creating..."
    rm $VAR_NETCONFIG_FILE
  fi

  #create cloud-init file
  echo "creating cloud config file..."
#create static networking if IPV4 is specified
if [ "$VAR_IPV4" != "" ] && [ "$VAR_NETCONFIG_FILE" != "" ]; then
cat <<EOF > $VAR_NETCONFIG_FILE
instance-id: $VAR_VM_NAME 
local-hostname: $VAR_VM_NAME
network:
  version: 2
  ethernets:
    $VAR_DEF_ADAPTER_NAME:
      dhcp4: false
      addresses:
        - $VAR_IPV4 
      routes:
        - to: default
          via: $VAR_GW4
      nameservers:
        addresses: [$VAR_DNS1,$VAR_DNS2]
EOF
fi
echo "#cloud-config" > $VAR_USERDATA_FILE
if [ "$VAR_OS_FAM" == "rhel" ]; then
cat <<EOF >> $VAR_USERDATA_FILE
local-hostname: $VAR_VM_NAME
network:
  version: 2
  ethernets:
    $VAR_DEF_ADAPTER_NAME:
      addresses:
        - $VAR_IPV4
      gateway4: $VAR_GW4
      nameservers:
        addresses:
          - $VAR_DNS1
          - $VAR_DNS2
EOF
fi
cat <<EOF >> $VAR_USERDATA_FILE
system_info:
  groups:
    - $VAR_ADMIN_GROUP
  default_user:
    name: $VAR_USERNAME
    home: /home/$VAR_USERNAME
    passwd: $VAR_PASSWORD
    groups: [$VAR_ADMIN_GROUP]
EOF
#handle ssh key vs. not
if [ "$VAR_SSH_KEY" != "" ]; then
cat <<EOF >> $VAR_USERDATA_FILE
    lock_passwd: true
    ssh_authorized_keys:
      - $VAR_SSH_KEY
ssh_pwauth: false
EOF
else
cat <<EOF >> $VAR_USERDATA_FILE
# no ssh key supplied, allow users logging in using password
ssh_pwauth: true
EOF
fi
#finish up the rest of the file
cat <<EOF >> $VAR_USERDATA_FILE
chpasswd:
  expire: false

hostname: $VAR_VM_NAME

packages:
  - qemu-guest-agent

runcmd:
  - echo "Defaults:%$VAR_ADMIN_GROUP !requiretty" > /etc/sudoers.d/1010_$VAR_ADMIN_GROUP
  - echo "%$VAR_ADMIN_GROUP  ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/1010_$VAR_ADMIN_GROUP
  - chmod 640 /etc/sudoers.d/1010_$VAR_ADMIN_GROUP
  - chown root:root /etc/sudoers.d/1010_$VAR_ADMIN_GROUP
  - systemctl start qemu-guest-agent
  - sleep 10
  - systemctl stop qemu-guest-agent && systemctl start qemu-guest-agent
EOF
  #DEBUG: display finished files
  if [ "$VAR_NETCONFIG_FILE" != "" ]; then
    cat $VAR_NETCONFIG_FILE
    chgrp $VAR_GROUP $VAR_NETCONFIG_FILE
    ls -lah $VAR_NETCONFIG_FILE
  fi
  cat $VAR_USERDATA_FILE
  chgrp $VAR_GROUP $VAR_USERDATA_FILE
  ls -lah $VAR_USERDATA_FILE
  echo -e "\ncheck cloud-init file schema..."
  #requires sudo permissions or to change group permissions on '/var/lib/cloud/instance'
  #echo "cloud-init schema --config-file $VAR_CLOUDINIT_FILE"
  #cloud-init schema --config-file $VAR_CLOUDINIT_FILE

  #VM/"Domain" creation
  #DEBUG: list all networks
  virsh net-list --all
  echo -e "\ncreating vm..."
  if [ "$VAR_NETCONFIG_FILE" != "" ]; then 
    echo "install cmd: virt-install --name $VAR_VM_NAME \
                                    --ram $VAR_MEM \
                                    --vcpus $VAR_CPU \
                                    --cpu host \
                                    --hvm \
                                    --disk path=$VAR_VM_DIR/root-disk.qcow2 \
                                    --os-variant=$VAR_OS \
                                    --network bridge=$VAR_NET_BRIDGE \
                                    --graphics none \
                                    --import \
                                    --noautoconsole \ 
                                    --cloud-init user-data=$VAR_USERDATA_FILE,network-config=$VAR_NETCONFIG_FILE" 
    virt-install --name $VAR_VM_NAME \
                 --ram $VAR_MEM \
                 --vcpus $VAR_CPU \
                 --cpu host \
                 --hvm \
                 --disk path=$VAR_VM_DIR/root-disk.qcow2 \
                 --os-variant=$VAR_OS \
                 --network bridge=$VAR_NET_BRIDGE \
                 --graphics none \
                 --import \
                 --noautoconsole \
                 --cloud-init user-data=$VAR_USERDATA_FILE,network-config=$VAR_NETCONFIG_FILE
  else
    echo "install cmd: virt-install --name $VAR_VM_NAME \
                                    --ram $VAR_MEM \
                                    --vcpus $VAR_CPU \
                                    --cpu host \
                                    --hvm \
                                    --disk path=$VAR_VM_DIR/root-disk.qcow2 \
                                    --os-variant=$VAR_OS \
                                    --network bridge=$VAR_NET_BRIDGE \
                                    --graphics none \
                                    --import \
                                    --noautoconsole \ 
                                    --cloud-init user-data=$VAR_USERDATA_FILE" 
    virt-install --name $VAR_VM_NAME \
                 --ram $VAR_MEM \
                 --vcpus $VAR_CPU \
                 --cpu host \
                 --hvm \
                 --disk path=$VAR_VM_DIR/root-disk.qcow2 \
                 --os-variant=$VAR_OS \
                 --network bridge=$VAR_NET_BRIDGE \
                 --graphics none \
                 --import \
                 --noautoconsole \
                 --cloud-init user-data=$VAR_USERDATA_FILE
  fi
  VAR_CHECK=$(virsh list --all | grep $VAR_VM_NAME)
  if [ "$VAR_CHECK" != "" ]; then
    #give it a couple of minutes to install qemu-guest-agent and restart
    sleep 60 
    #reboot (used in testing new use cases) 
    #virsh reboot $VAR_VM_NAME
    #sleep 5
    #virsh start $VAR_VM_NAME
    #sleep 60 
    echo "creation complete."
    VAR_IP4_ONLY=$(echo $VAR_IPV4 | awk -F'/' '{ print $1 }')
    #work around for RHEL (Fedora)
    if [ "$VAR_OS_FAM" == "ubuntu" ]; then
      sudo hostctl add domains $VAR_VM_NAME-test $VAR_VM_NAME --ip $VAR_IP4_ONLY
      hostctl list
    fi
    exit 0
  else
    echo "Error: domain not found."
    exit 1
  fi
fi
virsh list --all
