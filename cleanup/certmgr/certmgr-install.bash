#!/bin/bash

helm repo add jetstack https://charts.jetstack.io --force-update
helm repo update
#safe over easy method
#see https://cert-manager.io/docs/installation/helm/#crd-considerations
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.14.3/cert-manager.crds.yaml
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.14.3 

