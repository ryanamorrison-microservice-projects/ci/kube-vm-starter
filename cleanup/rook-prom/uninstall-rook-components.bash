#!/bin/bash

kubectl delete -f rook-localrules.yaml
kubectl delete -f rook-rbac.yaml
kubectl delete -f exporter-service-monitor.yaml
kubectl delete -f rook-service-monitor.yaml
