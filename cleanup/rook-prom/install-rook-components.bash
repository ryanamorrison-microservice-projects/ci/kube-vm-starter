#!/bin/bash
export KUBECONFIG=/etc/kubernetes/admin.conf

#https://rook.io/docs/rook/v1.9/Storage-Configuration/Monitoring/ceph-monitoring/#prometheus-instances
kubectl create -f service-monitor.yaml
kubectl create -f rbac.yaml
kubectl create -f prometheus.yaml
kubectl create -f prometheus-service.yaml

kubectl create -f csi-metrics-service-monitor.yaml -f exporter-service-monitor.yaml -f localrules.yaml


