#!/bin/bash
export KUBECONFIG=/etc/kubernetes/admin.conf
kubectl create namespace monitoring
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install monitoring prometheus-community/kube-prometheus-stack --namespace monitoring
sleep 15
kubectl get all -n monitoring
#optional until BGP
kubectl create -f service-nodeport.yaml
