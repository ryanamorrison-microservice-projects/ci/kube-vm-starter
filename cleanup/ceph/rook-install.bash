#!/bin/bash
export KUBECONFIG=/etc/kubernetes/admin.conf
kubectl create -f crds.yaml -f common.yaml -f operator.yaml
i=0
while : ; do
  check=$(kubectl -n rook-ceph get po -o wide)
  if [[ $check =~ "Running" ]]; then
    kubectl -n rook-ceph get po -o wide
    break
  fi
  if [ $i -gt 10]; then
    break
    exit 0
  fi
  sleep 15
  ((i=i+1))
done
kubectl create -f cluster.yaml
i=0
until [ $i -gt 10 ]; do
  kubectl -n rook-ceph get po -o wide
  sleep 60
  ((i=i+1))
done
kubectl create -f toolbox.yaml
kubectl -n rook-ceph get po -o wide
sleep 10 
kubectl -n rook-ceph exec deploy/rook-ceph-tools -- ceph status 
sleep 2
kubectl -n rook-ceph exec deploy/rook-ceph-tools -- ceph osd status 
sleep 2
kubectl -n rook-ceph exec deploy/rook-ceph-tools -- ceph df 
sleep 2
kubectl -n rook-ceph exec deploy/rook-ceph-tools -- rados df 

kubectl create -f ceph-dashboard-nodeport.yaml
kubectl -n rook-ceph get po -o wide
kubectl -n rook-ceph get service -o wide
