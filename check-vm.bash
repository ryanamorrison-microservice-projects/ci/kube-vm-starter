#!/bin/bash

echo "setting defaults..."
#script defaults
VAR_GROUP="libvirt-qemu"
VAR_IMAGE_DIR="/var/lib/libvirt/images"
VAR_TEMPLATE_DIR="/var/lib/libvirt/templates"
VAR_LOG_DIR="/var/log/libvirt/qemu/"

#set user defaults
VAR_VM_NAME="vm"
VAR_USERNAME="$USER"

#help display function
usage() {
  echo -e "Usage: $0 [-h] [-n name]\n"
  echo -e "  -h: display this help message\n"
  echo -e "  -n: name of host, the libvirt 'domain' (e.g., myhost)"
  echo -e "      defaults to: vm\n"
}

#get args
while getopts ":h:n:" opt; do
  case $opt in
    h ) usage
        exit 0
    ;;
    n ) VAR_VM_NAME="$OPTARG"
    ;;
   \? ) echo "Invalid option -$OPTARG" 1>&2
        usage
        exit 1
    ;;
    #: ) echo "Invalid option: -$OPTARG requires a value" 1>&2
    #    usage
    #    exit 1
    #;;
  esac
done

#set paths
VAR_VM_DIR="$VAR_IMAGE_DIR/$VAR_VM_NAME"

#check created VM
echo "showing xml for created libvirt domain $VAR_VM_NAME..."
virsh dumpxml $VAR_VM_NAME
echo ""

#check for an IP and MAC
echo "checking for IP address..."
virsh domifaddr --source agent $VAR_VM_NAME
VAR_FOUND_IP=$(virsh domifaddr --source agent $VAR_VM_NAME | grep '52:54:00' | awk -F' ' '{ print $NF }' | awk -F'/' '{ print $1 }')
if [ "$VAR_FOUND_IP" != "" ]; then
  echo "ping test..."
  ping $VAR_FOUND_IP -c 2
  echo $VAR_FOUND_IP > $VAR_VM_DIR/iface.ipv4
  echo ""
  #RHEL (Fedora) workaround: use DHCP
  VAR_HOSTS_CHECK=$(cat /etc/hosts | grep $VAR_VM_NAME)
  if [ "$VAR_HOSTS_CHECK" == "" ]; then
    echo "updating /etc/hosts..."
    sudo hostctl add domains $VAR_VM_NAME-test $VAR_VM_NAME --ip $VAR_FOUND_IP
    hostctl list
  fi
  echo "removing previous entries host's IP ($VAR_FOUND_IP) from the local known_hosts file..."
  ssh-keygen -R $VAR_FOUND_IP
  ssh-keygen -R $VAR_VM_NAME
  echo "ssh host keys for new host..."
  ssh-keyscan $VAR_FOUND_IP | ssh-keygen -lf -
  echo ""
  echo "adding ssh host keys to local known_hosts file..."
  VAR_HOST_KEY=$(ssh-keyscan -4 -t ed25519 $VAR_FOUND_IP | grep 'ssh-ed25519')
  echo $VAR_HOST_KEY >> $HOME/.ssh/known_hosts
  VAR_HOST_KEY=$(ssh-keyscan -4 -t ed25519  $VAR_VM_NAME | grep 'ssh-ed25519')
  echo $VAR_HOST_KEY >> $HOME/.ssh/known_hosts
  cat $HOME/.ssh/known_hosts | grep "$VAR_FOUND_IP"
  cat $HOME/.ssh/known_hosts | grep "$VAR_VM_NAME"
  echo ""
  echo "attempting ssh test..."
  VAR_USERNAME=$(cat $HOME/.ssh/$VAR_VM_NAME.pub | awk -F'--' '{ print $NF }')
  echo "command: ssh -i $HOME/.ssh/$VAR_VM_NAME $VAR_USERNAME@$VAR_FOUND_IP"
ssh -i $HOME/.ssh/$VAR_VM_NAME $VAR_USERNAME@$VAR_FOUND_IP << EOF
  echo "logged in:" 
  whoami
  echo "as sudo:" 
  sudo whoami
  exit
EOF
else
  "Error: no IPv4 was returned from the QEMU agent."
  exit 1
fi

echo "show running VM's..."
virsh list --all

echo "show log dir..."
ls -lah $VAR_LOG_DIR

#echo "sudo tail -100 /var/log/libvirt/qemu/$VAR_VM_NAME.log"
