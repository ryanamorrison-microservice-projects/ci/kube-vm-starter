#!/bin/bash

#script defaults
VAR_GROUP="libvirt-qemu"
VAR_IMAGE_DIR="/var/lib/libvirt/images"
VAR_TEMPLATE_DIR="/var/lib/libvirt/templates"


#set user defaults
VAR_VM_NAME="vm"
VAR_USERNAME="$USER"
VAR_DISK_SIZE_IN_GB="16"

#help display function
usage() {
  echo -e "Usage: $0 [-h] [-d disk size] [-l drive location] [-n name] \n"
  echo -e "  -d: disk size in Gigabytes (e.g. 20)"
  echo -e "      defaults to: 16\n"
  echo -e "  -h: display this help message\n"
  echo -e "  -l: a drive location (i.e., device such as vdb)"
  echo -e "      required, no default\n"
  echo -e "  -n: name of host, the libvirt 'domain' (e.g., myhost)"
  echo -e "      defaults to: vm\n"
}

#get args
while getopts ":d:h:l:n:" opt; do
  case $opt in
    h ) usage
        exit 0
    ;;
    d ) VAR_DISK_SIZE_IN_GB="$OPTARG" 
    ;;
    l ) VAR_DEVICE="$OPTARG"
    ;;
    n ) VAR_VM_NAME="$OPTARG"
    ;;
   \? ) echo "Invalid option -$OPTARG" 1>&2
        usage
        exit 1
    ;;
    #: ) echo "Invalid option: -$OPTARG requires a value" 1>&2
    #    usage
    #    exit 1
    #;;
  esac
done

virsh pool-list --all

virsh pool-info $VAR_VM_NAME

virsh domblklist $VAR_VM_NAME --details

VAR_NEW_DISK_NAME="${VAR_VM_NAME}_${VAR_DEVICE}_${VAR_DISK_SIZE_IN_GB}"
VAR_NEW_DISK_FILE="$VAR_IMAGE_DIR/$VAR_VM_NAME/$VAR_NEW_DISK_NAME"

#check for existing disk (do not clobber)
#made to work with Gitlab's `when: manual` syntax for extended testing
if [ -f $VAR_NEW_DISK_FILE ]; then
  echo "Found $VAR_NEW_DISK_FILE, skipping creation..."
else
  echo "No existing image found, creating..."
  echo "virsh vol-create-as $VAR_VM_NAME $VAR_NEW_DISK_NAME ${VAR_DISK_SIZE_IN_GB}GB"
  virsh vol-create-as $VAR_VM_NAME $VAR_NEW_DISK_NAME ${VAR_DISK_SIZE_IN_GB}GB 
  #limited sudo privs
  sudo chmod 660 $VAR_NEW_DISK_FILE
  sudo chgrp $VAR_GROUP $VAR_NEW_DISK_FILE
  sudo chown $VAR_USERNAME $VAR_NEW_DISK_FILE 

fi

#check to see if disk is already attached (do not clobber)
VAR_DISK_CHECK="$(virsh domblklist  $VAR_VM_NAME --details | grep $VAR_NEW_DISK_NAME)"
if [ "$VAR_DISK_CHECK" != "" ]; then
  echo "Disk is already attached, skipping attachment..."
else
  echo "virsh attach-disk --config ${VAR_VM_NAME} --source ${VAR_NEW_DISK_FILE} --target ${VAR_DEVICE} --persistent"
  virsh attach-disk --config ${VAR_VM_NAME} --source ${VAR_NEW_DISK_FILE} --target ${VAR_DEVICE} --persistent
  VAR_DISK_CHECK="$(virsh domblklist $VAR_VM_NAME --details | grep $VAR_NEW_DISK_NAME)"
  if [ "$VAR_DISK_CHECK" == "" ]; then
    echo "error attaching disk"
    virsh domblklist $VAR_VM_NAME --details
    ls -lah $VAR_NEW_DISK_FILE
    exit 1
  fi
  #reboot (used in testing new use cases)
  virsh reboot $VAR_VM_NAME
  sleep 5
  virsh start $VAR_VM_NAME
  sleep 60
fi

#additional disk inventory file
VAR_DISK_INV="$VAR_IMAGE_DIR/$VAR_VM_NAME/additional_disks"
if [ ! -f $VAR_DISK_INV ]; then
  echo "inventory file not found, creating..."
  touch $VAR_DISK_INV
  ls -lah $VAR_DISK_INV
  chgrp $VAR_GROUP $VAR_DISK_INV
fi

#update inventory if needed
VAR_FILE_CHECK="$(cat $VAR_DISK_INV | grep $VAR_NEW_DISK_NAME)"
if [ "$VAR_FILE_CHECK" == "" ]; then
  echo "adding disk to inventory file..."
  echo "$VAR_NEW_DISK_FILE" >> $VAR_DISK_INV
else
  echo "disk found in inventory file, skip adding it to the inventory..."
fi
echo "displaying additional disk inventory file..."
cat $VAR_DISK_INV

echo "displaying new disk file..."
ls -lah $VAR_NEW_DISK_FILE
echo " "
virsh domblklist $VAR_VM_NAME --details
