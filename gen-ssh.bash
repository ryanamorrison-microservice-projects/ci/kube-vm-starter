#!/bin/bash

#script defaults
VAR_GROUP="libvirt-qemu"
VAR_IMAGE_DIR="/var/lib/libvirt/images"
VAR_TEMPLATE_DIR="/var/lib/libvirt/templates"

#set user defaults
VAR_VM_NAME="vm"
VAR_USERNAME="$USER"
VAR_GENERATE_KEY="true"

#help display function
usage() {
  echo -e "Usage: $0 [-h] [-k true|false] [-n name] [-s ssh] [-u user] \n"
  echo -e "  -h: display this help message\n"
  echo -e "  -k: generate a temp ssh key"
  echo -e "      defaults to true\n"
  echo -e "  -n: name of host, the libvirt 'domain' (e.g., myhost)"
  echo -e "      defaults to: vm\n"
  echo -e "  -s: a 'quoted' public ssh key for the default user (e.g., 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDZ...')"
  echo -e "      if not specified, password authentication will be allowed"
  echo -e "      no default\n"
  echo -e "  -u: username of the initial user on the system"
  echo -e "      defaults to user account running script\n"
}

#get args
while getopts ":k:n::s:u:" opt; do
  case $opt in
    h ) usage
        exit 0
    ;;
    k ) VAR_GENERATE_KEY="$OPTARG"
    ;; 
    n ) VAR_VM_NAME="$OPTARG"
    ;;
    s)  VAR_SSH_KEY="$OPTARG"
    ;;
    u ) VAR_USERNAME="$OPTARG"
    ;;
   \? ) echo "Invalid option -$OPTARG" 1>&2
        usage
        exit 1
    ;;
    #: ) echo "Invalid option: -$OPTARG requires a value" 1>&2
    #    usage
    #    exit 1
    #;;
  esac
done

#set paths
VAR_VM_DIR="$VAR_IMAGE_DIR/$VAR_VM_NAME"

#ensure ssh 
if [ ! -d $HOME/.ssh ]; then
  mkdir $HOME/.ssh
  chown $USER:$USER $HOME/.ssh
  chmod 700 $HOME/.ssh
  ls -lahd $HOME/.ssh
fi
if [ ! -f $HOME/.ssh/known_hosts ]; then
  touch $HOME/.ssh/known_hosts
  chown $USER:$USER $HOME/.ssh/known_hosts
  chmod 600 $HOME/.ssh/known_hosts
  ls -lah $HOME/.ssh/known_hosts
fi

#generate a temp ssh key pair for accessing the host
if [ $VAR_GENERATE_KEY == "true" ] && [ ! -f $HOME/.ssh/$VAR_VM_NAME ]; then
  echo "generating temp ssh key for accessing the vm with user $VAR_USERNAME..."
  ssh-keygen -t ed25519 -q -N "" -f $HOME/.ssh/$VAR_VM_NAME -C "$VAR_VM_NAME--$VAR_USERNAME"
  VAR_SSH_KEY=$(cat $HOME/.ssh/$VAR_VM_NAME.pub)
  echo "public key: $VAR_SSH_KEY"
  ls -lah $HOME/.ssh/$VAR_VM_NAME
  ls -lah $HOME/.ssh/$VAR_VM_NAME.pub
else
  echo "ssh key generation skipped"
  if [ $VAR_GENERATE_KEY == "false" ]; then
    echo "no key generation requested"
  elif [ -f $HOME/.ssh/$VAR_VM_NAME ]; then
    echo "existing key found from a prior run"
  else
    echo "logical error"
    exit 1
  fi 
fi
