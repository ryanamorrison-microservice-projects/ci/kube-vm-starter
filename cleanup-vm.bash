#!/bin/bash

#script defaults
VAR_GROUP="libvirt-qemu"
VAR_IMAGE_DIR="/var/lib/libvirt/images"
VAR_TEMPLATE_DIR="/var/lib/libvirt/templates"

#set user defaults
VAR_VM_NAME="vm"

#help display function
usage() {
  echo -e "Usage: $0 [-h] [-n name]\n"
  echo -e "  -h: display this help message\n"
  echo -e "  -n: name of host, the libvirt 'domain' (e.g., myhost)"
  echo -e "      defaults to: vm\n"
}

#get args
while getopts ":h:n:" opt; do
  case $opt in
    h ) usage
        exit 0
    ;;
    n ) VAR_VM_NAME="$OPTARG"
    ;;
   \? ) echo "Invalid option -$OPTARG" 1>&2
        usage
        exit 1
    ;;
    #: ) echo "Invalid option: -$OPTARG requires a value" 1>&2
    #    usage
    #    exit 1
    #;;
  esac
done

#set paths
VAR_VM_DIR="$VAR_IMAGE_DIR/$VAR_VM_NAME"
VAR_USERDATA_FILE="$VAR_IMAGE_DIR/$VAR_VM_NAME/user-data.yaml"
VAR_NETCONFIG_FILE="$VAR_IMAGE_DIR/$VAR_VM_NAME/network-config.yaml"
VAR_DISK_INV="$VAR_IMAGE_DIR/$VAR_VM_NAME/additional_disks"

virsh list --all

VAR_FOUND_IP=$(cat $VAR_VM_DIR/iface.ipv4)

#cleanup
virsh shutdown $VAR_VM_NAME
virsh undefine $VAR_VM_NAME
virsh destroy $VAR_VM_NAME
virsh list --all

echo "removing build artifacts for $VAR_VM_DIR ..."
rm $VAR_VM_DIR/root-disk.qcow2
rm $VAR_USERDATA_FILE
rm $VAR_NETCONFIG_FILE
rm $VAR_VM_DIR/iface.ipv4

virsh pool-info $VAR_VM_NAME
virsh pool-destroy $VAR_VM_NAME
if [ -f $VAR_DISK_INV ]; then
  echo "displaying additional disk inventory from $VAR_DISK_INV ..."
  cat $VAR_DISK_INV
  echo "removing additional disks..."
  while read line; do
     echo "removing $line ..."
     rm $line
     virsh vol-delete --pool $VAR_VM_DIR $line
  done < $VAR_DISK_INV
  rm $VAR_DISK_INV
else
  echo "no additional disks found..."
fi
virsh pool-undefine $VAR_VM_NAME
virsh pool-list

rmdir $VAR_VM_DIR
ls -lah $VAR_IMAGE_DIR

echo "removing the host's IP ($VAR_FOUND_IP and $VAR_VM_NAME) from the local known_hosts file..."
ssh-keygen -R $VAR_FOUND_IP
ssh-keygen -R $VAR_VM_NAME

sudo hostctl remove $VAR_VM_NAME-test
hostctl list

echo "removing temporary ssh keys..."
rm $HOME/.ssh/$VAR_VM_NAME
rm $HOME/.ssh/$VAR_VM_NAME.pub 
ls -lah $HOME/.ssh

#to keep cruft from filling up the system for temp VM's
#requires sudo, investigate changing log location
#rm /var/log/libvirt/qemu/$VAR_VM_NAME.log

